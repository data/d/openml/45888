# OpenML dataset: padding-attack-dataset-2023-12-05-facebook-2023-12-05

https://www.openml.org/d/45888

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Bleichenbacher Padding Attack: Dataset created on 2023-12-05 with server 2023-12-05-facebook

Attribute Names:

CKE0:tcp.srcport
CKE0:tcp.dstport
CKE0:tcp.port
CKE0:tcp.stream
CKE0:tcp.len
CKE0:tcp.seq
CKE0:tcp.nxtseq
CKE0:tcp.ack
CKE0:tcp.hdr_len
CKE0:tcp.flags.res
CKE0:tcp.flags.ns
CKE0:tcp.flags.cwr
CKE0:tcp.flags.ecn
CKE0:tcp.flags.urg
CKE0:tcp.flags.ack
CKE0:tcp.flags.push
CKE0:tcp.flags.reset
CKE0:tcp.flags.syn
CKE0:tcp.flags.fin
CKE0:tcp.window_size_value
CKE0:tcp.window_size
CKE0:tcp.window_size_scalefactor
CKE0:tcp.checksum.status
CKE0:tcp.urgent_pointer
CKE0:tcp.options.nop
CKE0:tcp.option_kind
CKE0:tcp.option_len
CKE0:tcp.options.timestamp.tsval
CKE0:tcp.time_delta
CKE0:order
DISC0:tcp.srcport
DISC0:tcp.dstport
DISC0:tcp.port
DISC0:tcp.stream
DISC0:tcp.len
DISC0:tcp.seq
DISC0:tcp.nxtseq
DISC0:tcp.ack
DISC0:tcp.hdr_len
DISC0:tcp.flags.res
DISC0:tcp.flags.ns
DISC0:tcp.flags.cwr
DISC0:tcp.flags.ecn
DISC0:tcp.flags.urg
DISC0:tcp.flags.ack
DISC0:tcp.flags.push
DISC0:tcp.flags.reset
DISC0:tcp.flags.syn
DISC0:tcp.flags.fin
DISC0:tcp.window_size_value
DISC0:tcp.window_size
DISC0:tcp.window_size_scalefactor
DISC0:tcp.checksum.status
DISC0:tcp.urgent_pointer
DISC0:tcp.options.nop
DISC0:tcp.option_kind
DISC0:tcp.option_len
DISC0:tcp.options.timestamp.tsval
DISC0:tcp.time_delta
DISC0:order
DISC1:tcp.srcport
DISC1:tcp.dstport
DISC1:tcp.port
DISC1:tcp.stream
DISC1:tcp.len
DISC1:tcp.seq
DISC1:tcp.nxtseq
DISC1:tcp.ack
DISC1:tcp.hdr_len
DISC1:tcp.flags.res
DISC1:tcp.flags.ns
DISC1:tcp.flags.cwr
DISC1:tcp.flags.ecn
DISC1:tcp.flags.urg
DISC1:tcp.flags.ack
DISC1:tcp.flags.push
DISC1:tcp.flags.reset
DISC1:tcp.flags.syn
DISC1:tcp.flags.fin
DISC1:tcp.window_size_value
DISC1:tcp.window_size
DISC1:tcp.window_size_scalefactor
DISC1:tcp.checksum.status
DISC1:tcp.urgent_pointer
DISC1:tcp.options.nop
DISC1:tcp.option_kind
DISC1:tcp.option_len
DISC1:tcp.options.timestamp.tsval
DISC1:tcp.time_delta
DISC1:order
CKE1:tcp.srcport
CKE1:tcp.dstport
CKE1:tcp.port
CKE1:tcp.stream
CKE1:tcp.len
CKE1:tcp.seq
CKE1:tcp.nxtseq
CKE1:tcp.ack
CKE1:tcp.hdr_len
CKE1:tcp.flags.res
CKE1:tcp.flags.ns
CKE1:tcp.flags.cwr
CKE1:tcp.flags.ecn
CKE1:tcp.flags.urg
CKE1:tcp.flags.ack
CKE1:tcp.flags.push
CKE1:tcp.flags.reset
CKE1:tcp.flags.syn
CKE1:tcp.flags.fin
CKE1:tcp.window_size_value
CKE1:tcp.window_size
CKE1:tcp.window_size_scalefactor
CKE1:tcp.checksum.status
CKE1:tcp.urgent_pointer
CKE1:tcp.options.nop
CKE1:tcp.option_kind
CKE1:tcp.option_len
CKE1:tcp.options.timestamp.tsval
CKE1:tcp.time_delta
CKE1:order
DISC2:tcp.srcport
DISC2:tcp.dstport
DISC2:tcp.port
DISC2:tcp.stream
DISC2:tcp.len
DISC2:tcp.seq
DISC2:tcp.nxtseq
DISC2:tcp.ack
DISC2:tcp.hdr_len
DISC2:tcp.flags.res
DISC2:tcp.flags.ns
DISC2:tcp.flags.cwr
DISC2:tcp.flags.ecn
DISC2:tcp.flags.urg
DISC2:tcp.flags.ack
DISC2:tcp.flags.push
DISC2:tcp.flags.reset
DISC2:tcp.flags.syn
DISC2:tcp.flags.fin
DISC2:tcp.window_size_value
DISC2:tcp.window_size
DISC2:tcp.window_size_scalefactor
DISC2:tcp.checksum.status
DISC2:tcp.urgent_pointer
DISC2:tcp.options.nop
DISC2:tcp.option_kind
DISC2:tcp.option_len
DISC2:tcp.options.timestamp.tsval
DISC2:tcp.time_delta
DISC2:order
DISC3:tcp.srcport
DISC3:tcp.dstport
DISC3:tcp.port
DISC3:tcp.stream
DISC3:tcp.len
DISC3:tcp.seq
DISC3:tcp.nxtseq
DISC3:tcp.ack
DISC3:tcp.hdr_len
DISC3:tcp.flags.res
DISC3:tcp.flags.ns
DISC3:tcp.flags.cwr
DISC3:tcp.flags.ecn
DISC3:tcp.flags.urg
DISC3:tcp.flags.ack
DISC3:tcp.flags.push
DISC3:tcp.flags.reset
DISC3:tcp.flags.syn
DISC3:tcp.flags.fin
DISC3:tcp.window_size_value
DISC3:tcp.window_size
DISC3:tcp.window_size_scalefactor
DISC3:tcp.checksum.status
DISC3:tcp.urgent_pointer
DISC3:tcp.options.nop
DISC3:tcp.option_kind
DISC3:tcp.option_len
DISC3:tcp.options.timestamp.tsval
DISC3:tcp.time_delta
DISC3:order
vulnerable_classes [0X00 In Pkcs#1 Padding (First 8 Bytes After 0X00 0X02), 0X00 In Some Padding Byte, Correctly Formatted Pkcs#1 Pms Message  But 1 Byte Shorter, No 0X00 In Message, Wrong First Byte (0X00 Set To 0X17), Wrong Second Byte (0X02 Set To 0X17)]

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45888) of an [OpenML dataset](https://www.openml.org/d/45888). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45888/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45888/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45888/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

